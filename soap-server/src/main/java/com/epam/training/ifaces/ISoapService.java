package com.epam.training.ifaces;

import com.epam.training.beans.Person;
import com.epam.training.exceptions.NoFriendsException;
import com.epam.training.exceptions.NoPersonException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface ISoapService {
    @WebMethod
    List<Person> getPersonFriends(@WebParam(name="person") Person person, @WebParam(name="year") int year) throws NoPersonException, NoFriendsException;
}
