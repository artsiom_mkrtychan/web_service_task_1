package com.epam.training.impls;

import com.epam.training.beans.Person;
import com.epam.training.exceptions.NoFriendsException;
import com.epam.training.exceptions.NoPersonException;
import com.epam.training.ifaces.ISoapService;

import javax.jws.WebService;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static com.epam.training.constants.Constants.*;

@WebService(endpointInterface = "com.epam.training.ifaces.ISoapService", serviceName = "SoapServiceImpl")
public class SoapServiceImpl implements ISoapService {

    public List<Person> getPersonFriends(Person person, int year) throws NoPersonException, NoFriendsException {

        List<Person> resultFriendsList = new LinkedList<Person>();
        int indexOfPerson = PERSON_LIST.indexOf(person);

        if (indexOfPerson != PERSON_IN_LIST) {
            Person personFromList = PERSON_LIST.get(indexOfPerson);
            List<Person> personFriends = personFromList.getFriends();

            if (personFriends.size() != NO_FRIENDS) {
                for (Person friend : personFriends) {

                    Date friendBirthday = friend.getBirthday();
                    int friendBirthYear = friendBirthday.getYear() + DATE_CORRECTION;

                    if (friendBirthYear == year) {
                        resultFriendsList.add(friend);
                    }
                }
            } else {
                throw new NoFriendsException(NO_FRIENDS_MSG);
            }

        } else {
            throw new NoPersonException(NO_PERSON_MSG);
        }

        if (resultFriendsList.size() == NO_FRIENDS) {
            throw new NoFriendsException(NO_SUCH_FRIEND_MSG);
        }

        return resultFriendsList;
    }
}
