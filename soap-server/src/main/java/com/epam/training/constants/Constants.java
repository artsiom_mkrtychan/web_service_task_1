package com.epam.training.constants;

import com.epam.training.beans.Person;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Constants {

    public final static String SOAP_SERVICE_ADRESS = "/soap_service";
    public final static String DEFAULT_PERSON_NAME = "serj";
    public final static String NO_FRIENDS_MSG = "No any friend";
    public final static String NO_PERSON_MSG = "No such person";
    public final static String NO_SUCH_FRIEND_MSG = "Person has no appropriate friend";
    public final static int PERSON_IN_LIST = -1;
    public final static int NO_FRIENDS = 0;
    public final static int DATE_CORRECTION = 1900;
    public final static List<Person> PERSON_LIST;


    static {
        Person firstPerson = new Person();
        Person secondPerson = new Person();
        Person thirdPerson = new Person();
        Person fourthPerson = new Person();
        Person fifthPerson = new Person();

        firstPerson.setName("john");
        secondPerson.setName("lew");
        thirdPerson.setName("born");
        fourthPerson.setName("torn");
        fifthPerson.setName("craig");

        firstPerson.setBirthday(new Date(90,0,10));
        secondPerson.setBirthday(new Date(80,1,12));
        thirdPerson.setBirthday(new Date(70,2,14));
        fourthPerson.setBirthday(new Date(60,3,16));
        fifthPerson.setBirthday(new Date(50,4,18));

        List<Person> firstPersonFriends = new ArrayList<Person>();
        List<Person> secondPersonFriends = new ArrayList<Person>();
        List<Person> thirdPersonFriends = new ArrayList<Person>();

        //init persons friends
        firstPersonFriends.add(secondPerson);
        firstPersonFriends.add(thirdPerson);

        secondPersonFriends.add(thirdPerson);
        secondPersonFriends.add(fourthPerson);

        thirdPersonFriends.add(fourthPerson);
        thirdPersonFriends.add(fifthPerson);

        firstPerson.setFriends(firstPersonFriends);
        secondPerson.setFriends(secondPersonFriends);
        thirdPerson.setFriends(thirdPersonFriends);


        PERSON_LIST = new ArrayList<Person>();
        PERSON_LIST.add(firstPerson);
        PERSON_LIST.add(secondPerson);
        PERSON_LIST.add(thirdPerson);

    }





}
