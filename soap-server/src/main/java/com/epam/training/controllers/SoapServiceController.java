package com.epam.training.controllers;

import com.epam.training.constants.Constants;
import com.epam.training.impls.SoapServiceImpl;
import org.apache.cxf.Bus;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;

import javax.servlet.ServletConfig;
import javax.servlet.annotation.WebServlet;

@WebServlet(name="SoapServiceController", urlPatterns = "/*")
public class SoapServiceController extends CXFNonSpringServlet {

    @Override
    public void loadBus(ServletConfig servletConfig){

        super.loadBus(servletConfig);
        Bus bus = getBus();

        ServerFactoryBean factory = new ServerFactoryBean();
        factory.setBus(bus);
        factory.setServiceClass(SoapServiceImpl.class);
        factory.setAddress(Constants.SOAP_SERVICE_ADRESS);
        factory.create();
    }
}
