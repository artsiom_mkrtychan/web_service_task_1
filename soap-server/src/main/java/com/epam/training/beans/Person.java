package com.epam.training.beans;

import com.epam.training.constants.Constants;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement(name = "Person")
public class Person {
    private String name;
    private Date birthday;
    private List<Person> friends;

    public Person() {
        this.name = Constants.DEFAULT_PERSON_NAME;
        this.birthday = new Date(1990, 1, 1);
        this.friends = new ArrayList<Person>();
    }

    public Person(String name, Date birthday, List<Person> friends) {
        this.name = name;
        this.birthday = birthday;
        this.friends = friends;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public List<Person> getFriends() {
        return friends;
    }

    public void setFriends(List<Person> friends) {
        this.friends = friends;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (name != null ? !name.equals(person.name) : person.name != null) return false;
        return birthday != null ? birthday.equals(person.birthday) : person.birthday == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        return result;
    }
}
