package com.epam.training.exceptions;

public class NoFriendsException extends Exception {
    public NoFriendsException() {
        super();
    }

    public NoFriendsException(String message) {
        super(message);
    }

    public NoFriendsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoFriendsException(Throwable cause) {
        super(cause);
    }
}
