package com.epam.training.exceptions;

public class NoPersonException extends Exception {
    public NoPersonException() {
        super();
    }

    public NoPersonException(String message) {
        super(message);
    }

    public NoPersonException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoPersonException(Throwable cause) {
        super(cause);
    }
}
