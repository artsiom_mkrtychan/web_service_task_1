import com.epam.training.impls.*;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class SoapClient {

    public static void main(String[] args) throws DatatypeConfigurationException {

        SoapServiceImpl service = new SoapServiceImpl();
        SoapServiceImplPortType portType = service.getSoapServiceImplPort();

        Person person = new Person();
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(new Date(90, 0, 10));
        XMLGregorianCalendar date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        person.setName("john");
        person.setBirthday(date2);

        try {

            System.out.println("\n \nCorrect request");
            List<Person> persons = portType.getPersonFriends(person, 1980);
            for (Person tempPerson : persons) {
                System.out.println("name: " + tempPerson.getName());
                System.out.println("birthday: " + tempPerson.getBirthday() + "\n");
            }

            System.out.println("Request with error (custom exception from service reason of error)");
            portType.getPersonFriends(person, 1950);

        } catch (NoFriendsException_Exception e) {
            e.printStackTrace();
        } catch (NoPersonException_Exception e) {
            e.printStackTrace();
        }
    }
}
