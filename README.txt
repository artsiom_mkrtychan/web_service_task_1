Please use "mvn clean install tomcat7:run-war" on soap-server pom.xml and
           "mvn clean install exec:java" on soap-client pom.xml (will be shown
                                                                 correct result and
                                                                 result with error, 
                                                                 please see above the 
                                                                 error message).

Default wsdl address "http://localhost:8080/soap-server/soap_service?wsdl"